# PHP-Mitschrift

## KW 38: 

### Anlegen der Mitschrift
Entweder als Markdown-Datei oder Wiki im Repository. Jedes Wiki hat jedoch sein eigenes Git Repository und sollte als eigenes Projekt behandelt werden (siehe [GitLab Doks](https://docs.gitlab.com/ee/user/project/wiki/). Die Markdown-Datei kann in VSCode bearbeitet und dargestellt werden (z.B README.md).

### Grundlagen der PHP-Programmierung
Code kann von https://gitlab.com/meos/php_grundlagen_2023-24 geklont werden.

