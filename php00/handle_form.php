<?php

require "validation.php"; // Alternative include

if (isset($_POST)) {
    $name = htmlspecialchars($_POST['name']);
    $email = htmlspecialchars($_POST['email']);

    if (validate($name, $email)) {
        echo "Name: " . $name . "<br>";
        echo "Email: " . $email;
    } else {
        echo "<div><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
        foreach ($errors as $key => $value) {
            echo "<li>" . $value . "</li>";
        }
        echo "</ul></div>";
    }
}
