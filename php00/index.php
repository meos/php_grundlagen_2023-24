<?php

echo "Hallo POS1-Gruppe";

// Variablen
$firstName = "Max";
$lastName = "Mustermann";
$age = 30;
$isMarried = false;

// Datentypen
$s = "Hello, world!";
$i = 42;
$d = 3.14;
$n = null;

// Arithmetische Operatoren
$x = 10;
$y = 20;
$sum = $x + $y;
$diff = $x - $y;
$product = $x * $y;
$quotient = $x / $y;
$modulus = $x % $y;

// Konstanten
const PI = 3.14159265359;
define("E", 2.71828);

// Vergleihsoperatoren
$x = 10;
$y = 20;
var_dump($x == $y);  // false
var_dump($x != $y);  // true
var_dump($x < $y);   // true
var_dump($x > $y);   // false
var_dump($x <= $y);  // true
var_dump($x >= $y);  // false
var_dump($x === $y); // Vergleicht auch Datentyp!

// If-Anweisungen
$temperatur = 5;
if ($age < 10) {
    echo "Es ist kalt.";
} else {
    echo "Es ist warm.";
}

// ternärer Operator: wenn Bedingung ? dann TRUE : sonst FALSE 
// Verknüpfung mit &&, || (und !)
// auch switch

// Schleifen
for ($i = 0; $i < 5; $i++) {
    echo $i . "<br>"; // Konkatenation mit .-Operator
}

// auch while, do-while mit break, continue

// Arrays
$array = array(1, 2, 3, 4, 5);
$array1 = [6, 7, 8, 9];

// assoziative Arrays
$age = array("Peter" => 35, "Berni" => 37, "Josef" => 43);

// Zugriff über [index]!
// unset() fürs Zurücksetzen

// foreach-Schleife für Arrays
foreach ($array as $wert) {
    echo $wert . ", ";
}
foreach ($age as $name => $wert) {
    echo $name . " ist " . $wert . " Jahre alt.<br>";
 }

// Funktionen
$n = 12;
function greet($number)
{
    $number = $number + 2;
    return $number;
}

echo greet($n);
echo $n;

// call-by-value vs. call-by-reference! -> &
